(function($){
    //alert($('#category').val());
    if($('#category').val() != undefined && $('#category').val() != ""){
        // alert($('#category').val());
        $('.category-dropdown').val($('#category').val());
    }

    $('.show-more-text').click(function(){    

        if($(this).html().indexOf("More") >= 0){
            $('.show-more-color').addClass("show-less-color");
            $('.show-less-color').removeClass("show-more-color");
            $(this).html("<p>- Show Less Colors</p>")
        }
        else if($(this).html().indexOf("Less") >= 0 ){           
            $('.show-less-color').addClass("show-more-color");
            $('.show-more-color').removeClass("show-less-color");
            $(this).html("<p>+ Load More Colors</p>")
        }
    });
    
    $('.category-dropdown').on('change', function() {
        $url = this.value;
        var text1 = $('.category-dropdown option:selected').text();

        window.location=$url;
    });
    
    $('#btn_reset_filter').on('click', function() {
        FWP.reset();
    });
    
    $(document).on('click','.open_sidebar',function(){
         if ($(window).width() <= 768) {
            $('.product-filter').css('display', 'block');
            $('.category-dropdown').css('display', 'block');

            $('.filter').addClass('mobile-view-filter');

            $(this).find('button').text('Close Filter x');
            $(this).removeClass('open_sidebar');
            $(this).addClass('close_sidebar_button');
         }
    });
    
    $(document).on('click','.close_sidebar_button',function(){
         if ($(window).width() <= 768) {
            $('.product-filter').css('display', 'none');
            $('.category-dropdown').css('display', 'none');

            $('.filter').removeClass('mobile-view-filter');

            $(this).find('button').html('Filter <i class="fa fa-list-ul"></i>');
            $(this).addClass('open_sidebar');
            $(this).removeClass('close_sidebar_button');
         }
    });  
    
    function getUrlParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) 
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) 
            {
                return sParameterName[1];
            }
        }
    } 

    dataIndex = getUrlParameter("index");
    if(dataIndex !== undefined){        
        if(dataIndex < $('.fl-tabs-labels .fl-tabs-label').length){
            $('.fl-tabs-labels .fl-tabs-label,.fl-tabs-panels .fl-tabs-panel-content').removeClass("fl-tab-active");  
            tabs = $('.fl-tabs-labels .fl-tabs-label')[dataIndex];
            tabPanel = $('.fl-tabs-panels .fl-tabs-panel-content')[dataIndex];        
            $(tabs).addClass("fl-tab-active");
            $(tabPanel).addClass("fl-tab-active");
        }
    }
    
    tab = getUrlParameter("tab");
    
    if(tab !== undefined){ 
        $('#'+tab).parent().parent().addClass("fl-tab-active");
    }
    
    product_url = document.referrer;
    
    page = getUrlParameter("page");
    
    
    if(page !== undefined && page == 'schedule_appointment'){
         username = getUrlParameter("username");
        $('#input_7_11').val(username);
        if(username == ''){
            $('#input_7_10').val(product_url);
        }
    }
    
    if(page !== 'undefined' && page == 'repsale_contact'){
         username = getUrlParameter("username");
        $('#input_5_10').val(username);
    }
    
    if(page !== 'undefined' && page == 'commercail_contact'){
        username = getUrlParameter("username");
        $('#input_1_16').val(username);
    }
    
	if($(window).width() < 768){
		$('.navbar-toggle').click(function(){
			$('.fl-page-header-logo').toggleClass('unactiveLogo');
			$('.fl-page-nav-search').toggleClass('hide');
			$('.navbar-toggle .fa ').toggleClass('fa-bars fa-close');

		})

		$('.fl-page-nav-search .fa-search').click(function(){
			$('.fl-page-header-logo').toggleClass('unactiveLogo');
			$('.fl-page-nav-wrap').toggleClass('hide');
		})
	}
	setInterval(function(){
		if($(window).width() < 768){
			if($('.fl-tabs-label:last-child').hasClass('fl-tab-active')){
				// 			console.log('test')
				$('.tabToSlider .fl-tabs-labels .fl-tabs-label:first-child').trigger( "click" )		
			}else if(!$('.fl-tabs-label:last-child').hasClass('fl-tab-active')){
				// 			console.log('test2')

				$('.tabToSlider .fl-tabs-label.fl-tab-active').next().trigger( "click" )		
			}	
		}
		
	}, 5000);
	
	
	
})(jQuery);