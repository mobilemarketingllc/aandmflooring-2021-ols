<div class="product-attributes">
    <h3>PRODUCT SPECTS</h3>
     <table class="table table-striped">
        <thead>
        <tbody>
            <?php if(get_field('carpet_style')) { ?>
                <tr>
                    <th scope="row">Carpet Style</th>
                    <td><?php the_field('carpet_style'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('fiber')) { ?>
                <tr>
                    <th scope="row">Fiber</th>
                    <td><?php the_field('fiber'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('lifestyle')) { ?>
                <tr>
                    <th scope="row">Lifestyle</th>
                    <td><?php the_field('lifestyle'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('pattern_repeat')) { ?>
                <tr>
                    <th scope="row">Pattern Repeat</th>
                    <td><?php the_field('pattern_repeat'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('roll_width')) { ?>
                <tr>
                    <th scope="row">Roll Width</th>
                    <td><?php the_field('roll_width'); ?></td>
                </tr>
            <?php } ?>
           
            
            
            <?php if(get_field('shade')) { ?>
                <tr>
                    <th scope="row">Shade</th>
                    <td><?php the_field('shade'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('construction')) { ?>
                <tr>
                    <th scope="row">Construction</th>
                    <td><?php the_field('construction'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('installation_type')) { ?>
                <tr>
                    <th scope="row">Installation Type</th>
                    <td><?php the_field('installation_type'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('look')) { ?>
                <tr>
                    <th scope="row">Look</th>
                    <td><?php the_field('look'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('plank_width')) { ?>
                <tr>
                    <th scope="row">Plank Width</th>
                    <td><?php the_field('plank_width'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('thickness')) { ?>
                <tr>
                    <th scope="row">Thickness</th>
                    <td><?php the_field('thickness'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('diy_level')) { ?>
                <tr>
                    <th scope="row">DIY Level</th>
                    <td><?php the_field('diy_level'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('edge_detail')) { ?>
                <tr>
                    <th scope="row">Edge Detail</th>
                    <td><?php the_field('edge_detail'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('installation_information')) { ?>
                <tr>
                    <th scope="row">Installation Information</th>
                    <td><a href="<?php the_field('installation_information'); ?>" target="_blank">View</a></td>
                </tr>
            <?php } ?>
            <?php if(get_field('brand_information')) { ?>
                <tr>
                    <th scope="row">Brand Information</th>
                    <td><a href="<?php the_field('brand_information'); ?>" target="_blank">View</a></td>
                </tr>
            <?php } ?>
            
            
            
            <?php if(get_field('stone_type')) { ?>
                <tr>
                    <th scope="row">Stone Type</th>
                    <td><?php the_field('stone_type'); ?></td>
                </tr>
            <?php } ?>
            
            <?php if(get_field('product_size')) { ?>
                <tr>
                    <th scope="row">Product Size</th>
                    <td><?php the_field('product_size'); ?></td>
                </tr>
            <?php } ?>
            
            <?php if(get_field('tile_shape')) { ?>
                <tr>
                    <th scope="row">Tile Shape</th>
                    <td><?php the_field('tile_shape'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('tile_type')) { ?>
                <tr>
                    <th scope="row">Tile Type</th>
                    <td><?php the_field('tile_type'); ?></td>
                </tr>
            <?php } ?>
            
            <?php if(get_field('installation_location')) { ?>
                <tr>
                    <th scope="row">Installation Location</th>
                    <td><?php the_field('installation_location'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('wood_look')) { ?>
                <tr>
                    <th scope="row">Wood Look</th>
                    <td><?php the_field('wood_look'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('installation_type')) { ?>
                <tr>
                    <th scope="row">Installation Type</th>
                    <td><?php the_field('installation_type'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('mosaic')) { ?>
                <tr>
                    <th scope="row">Mosaic</th>
                    <td><?php the_field('mosaic'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('finish')) { ?>
                <tr>
                    <th scope="row">Finish</th>
                    <td><?php the_field('finish'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('collection')) { ?>
                <tr>
                    <th scope="row">Collection</th>
                    <td><?php the_field('collection'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('data_sheet')) { ?>
                <tr>
                    <th scope="row">Data Sheet</th>
                    <td><?php the_field('data_sheet'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('data_sheet_info')) { ?>
                <tr>
                    <th scope="row">Data Sheet Info</th>
                    <td><a href="<?php the_field('data_sheet_info'); ?>" target="_blank">View</a></td>
                </tr>
            <?php } ?>
            <?php if(get_field('shade_variation')) { ?>
                <tr>
                    <th scope="row">Shade Variation</th>
                    <td><?php the_field('shade_variation'); ?></td>
                </tr>
            <?php } ?>
            
            
            
            <?php if(get_field('style')) { ?>
                <tr>
                    <th scope="row">Style</th>
                    <td><?php the_field('style'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('installation_guide')) { ?>
                <tr>
                    <th scope="row">Installation Guide</th>
                    <td><?php the_field('installation_guide'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('product_line')) { ?>
                <tr>
                    <th scope="row">Product Line</th>
                    <td><?php the_field('product_line'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('width_x_lenth')) { ?>
                <tr>
                    <th scope="row">Width x Lenth</th>
                    <td><?php the_field('width_x_lenth'); ?></td>
                </tr>
            <?php } ?>
            <?php if(get_field('installation_method')) { ?>
                <tr>
                    <th scope="row">Installation Method</th>
                    <td><?php the_field('installation_method'); ?></td>
                </tr>
            <?php } ?>
            
            <?php if(get_field('installation')) { ?>
                <tr>
                    <th scope="row">Installationn</th>
                    <td><?php the_field('installation'); ?></td>
                </tr>
            <?php } ?>
            
            <?php if(get_field('tile_thickness')) { ?>
                <tr>
                    <th scope="row">Tile Thickness</th>
                    <td><?php the_field('tile_thickness'); ?></td>
                </tr>
            <?php } ?>
            
            
            
            
            
            
            
            <?php if(get_field('warrantly_info')) { ?>
                <tr>
                    <th scope="row">Warranty</th>
                    <td><?php the_field('warrantly_info'); ?></td>
                </tr>
            <?php } ?>
            
            <?php if(get_field('warranty_link')) { ?>
                <tr>
                    <th scope="row">Warranty Link</th>
                    <td><a href="<?php the_field('warranty_link'); ?>" target="_blank">View</a></td>
                </tr>
            <?php } ?>
                        
        </tbody>
    </table>
    </div>