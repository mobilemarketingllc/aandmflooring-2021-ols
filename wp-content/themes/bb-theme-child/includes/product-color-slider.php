<div class="productDName"><?php the_title();?></div>
<div class="product-variations">
    <div class="row">
        <?php
        $familysku = get_post_meta($post->ID, 'collection', true);
        if(is_singular( 'laminate' )){
            $flooringtype = 'laminate';
        } elseif(is_singular( 'hardwood' )){
            $flooringtype = 'hardwood';
        } elseif(is_singular( 'carpeting' )){
            $flooringtype = 'carpeting';
        } elseif(is_singular( 'luxury_vinyl_tile' )){
            $flooringtype = 'luxury_vinyl_tile';
        } elseif(is_singular( 'vinyl' )){
            $flooringtype = 'vinyl';
        } elseif(is_singular( 'solid_wpc_waterproof' )){
            $flooringtype = 'solid_wpc_waterproof';
        } elseif(is_singular( 'tile' )){
            $flooringtype = 'tile';
        }

        $args = array(
            'post_type'      => $flooringtype,
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'meta_query'     => array(
                array(
                    'key'     => 'collection',
                    'value'   => $familysku,
                    'compare' => '='
                )
            )
        );
        ?>
        <?php
        $the_query = new WP_Query( $args );
        ?>


        <div class="fr-slider color_variations_slider" data-fr='{"slidesToScroll":7,"slidesToShow":7,"arrows":false,"infinite": false}'>
            <a href="#" class="arrow prev"><img src="/wp-content/uploads/2018/04/arrow-left.png"></a>
            <a href="#" class="arrow next"><img src="/wp-content/uploads/2018/04/arrow-right.png"></a>
            <div class="slides">
                <?php  while ( $the_query->have_posts() ) {
                $the_query->the_post(); ?>
                    <div class="slide color-box">
                        <a  href="<?php the_permalink(); ?>">
                            <?php if (get_field('brand') == 'Quick-Step') { ?>
                                <img src="<?php the_field('swatch_image_link'); ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
                            <?php } else { ?>
							   <?php  if(get_field('swatch_image_link')){?>            
                                <img src="https://mm-media-res.cloudinary.com/image/fetch/h_100,w_100/<?php the_field('swatch_image_link'); ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
							<?php }  else { ?>
							 
							<img src="https://placehold.it/100x100?text=NO+IMAGE" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
							<?php } ?>
                            <?php } ?>



                        </a>
                        
                    </div>
                <?php } ?>
            </div>
        </div>

    <?php wp_reset_postdata(); ?>


</div>
    </div>