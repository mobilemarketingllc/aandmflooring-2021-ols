<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

add_action( 'wp_enqueue_scripts', function(){
wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/resources/slick/script.js","","",1);
wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
wp_enqueue_style("child-style",get_stylesheet_directory_uri()."/style.css","","",30);
});

// Classes
require_once 'classes/fl-class-child-theme.php';



add_action( 'after_setup_theme',     'FLChildTheme::setup' );
//add_action( 'init',                  'FLChildTheme::init_woocommerce' );
add_action( 'wp_enqueue_scripts',    'FLChildTheme::enqueue_scripts', 999 );
add_action( 'widgets_init',          'FLChildTheme::widgets_init' );
add_action( 'wp_footer',             'FLChildTheme::go_to_top' );

// Theme Filters
add_filter( 'body_class',            'FLChildTheme::body_class' );
add_filter( 'excerpt_more',          'FLChildTheme::excerpt_more' );

// Actions
//add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 ); 

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}
?>